<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Doctor;
use App\Models\Profile;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;

use function GuzzleHttp\Promise\all;

class DoctorController extends Controller
{
    public function store(Request $request)
    {

        $data=$request->all();
        $path='public/products';
        $image=$request->file('image');
        $image_name=time().$image->getClientOriginalName();
         $request->file('image')->storeAs($path,$image_name);


        $data['role_id']=3;
        $data['image']=$image_name;



        try {
            
           $user= User::create([
             'first_name'=>$data['fname'],
             'last_name'=>$data['lname'],
             'email'=>$data['email'],
             'password'=>$data['password'],
             'department_id'=>$data['department'],
             'role_id'=>$data['role_id'],

            ]);

            $user_id=$user->id;
            $profileData=[];
            $profileData['user_id']=$user_id;
            $profileData['gender']=$data['gender'];
            $profileData['phone']=$data['phone'];
            $profileData['dob']=$data['dob'];
            $profileData['nid']=$data['nid'];
            $profileData['degree']=$data['degree'];
            $profileData['join_date']=$data['join_date'];
            $profileData['city']=$data['city'];
            $profileData['state']=$data['state'];
            $profileData['address']=$data['address'];
            $profileData['bio']=$data['bio'];
            $profileData['image']=$data['image'];

            Profile::create($profileData);



        } catch (Exception $ex) {
            dd($ex);
        }
        return redirect()->route('doctors.index')->withMessage('Successfully Saved');
    }

    public function index()
    {
         $role_id=3;
        $doctors=User::where('role_id',3)->get();
     return view('backend.admin.doctors.index',compact('doctors'));

    }
    public function show($id){

        $doctor=User::findOrFail($id);
        return view('backend.admin.doctors.show',compact('doctor'));
    }

    public function create(){
        $data['departments']=Department::all();
        return view('backend.admin.doctors.create', $data);
    }

    public function edit($id){
      $doctor=User::findOrFail($id);
      return view('backend.admin.doctors.edit',compact('doctor'));
    }
    public function update(Request $request, $id){

        $data=$request->all();
        $doctor_update=User::findOrFail($id);
        $doctor_update->update([
            'first_name' => $data['fname'],
            'last_name' => $data['lname'],
            'email' => $data['email'],
            
        ]);
        $doctor_update->profile->update([
            'phone' => $data['phone'],
            'nid'=>$data['nid'],
            'degree' => $data['degree'],
            'address' => $data['address'],
            'bio'=>$data['bio'],
        ]);
   
        return redirect()->route('doctors.index')->withMessage("Successfully Upadeted Doctors Profile");

    }

}
