<x-backend.layouts.master>

    <div class="main-content container-fluid">
        <div class="page-title">
            <div class="row">
                <div class="col-12 col-md-6 order-md-1 order-last">
                    <h3>Appointment List</h3>

                </div>
                <div class="col-12 col-md-6 order-md-2 order-first">
                    <nav aria-label="breadcrumb" class='breadcrumb-header'>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Appointment</li>
                            <li class="breadcrumb-item active" aria-current="page">Appointment List</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        @if (session('message'))
            <p class="alert alert-secondary">{{ session('message') }}</p>
        @endif
        <section class="section">
            <div class="card">
                <div class="card-header">
                </div>
                <div class="card-body">
                    <table class="table table-striped table-sm table-hover table-light" id="table1">
                        <thead class="text-center">
                            <tr>
                                <th>SL</th>
                                <th>Patients Name</th>
                                <th>Phone</th>
                                <th>Date</th>
                                <th>Doctor Name</th>
                                <th>Status</th>
                                <th>Action</th>

                            </tr>
                        </thead>
                        <tbody class="text-center">

                            @foreach ($appointments as $appointment)
                                <tr>
                                    <td>{{ $loop->iteration }} </td>
                                    <td>{{ $appointment->patients_name }}</td>
                                    <td>{{ $appointment->phone }}</td>
                                    <td>{{ $appointment->appointment_date }}</td>
                                    <td>{{ $appointment->user->first_name . ' ' . $appointment->user->last_name }}</td>
                                    <td>{{ $appointment->status }}</td>

                                    <td>
                                        @if (!$appointment->approval_status)
                                            <form
                                                action="{{ route('update.approval.status', ['appointment' => $appointment->id]) }}"
                                                method="post" style="display:inline">
                                                @csrf
                                                @method('patch')
                                                <button type="submit" class="btn btn-info btn-sm">Approve</button>

                                            </form>
                                        @endif
                                        @if ($appointment->approval_status)
                                            <a class="btn btn-secondary btn-sm disabled">Approved</a>
                                        @endif
                                        <a class="btn btn-primary btn-sm" href="" data-toggle="modal"
                                            data-target="#exampleModal">Modify</a>
                                        <form
                                            action="{{ route('delete.appointment', ['appointment' => $appointment->id]) }}"
                                            method="post" style="display:inline">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-warning btn-sm">Cancel</button>

                                        </form>

                                    </td>

                                </tr>
                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content bg-gradient  ">
                                            <div class="modal-header ">
                                                <h5 class="modal-title text-center w-100 " id="exampleModalLabel">Edit
                                                    Appointment</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>

                                            <form
                                                action="{{ route('appointment.update', ['appointment' => $appointment->id]) }}"
                                                method="post">
                                                @csrf
                                                @method('patch')
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col">
                                                            <label for="patients_name">Patients Name</label>
                                                            <input type="text" class="form-control" id="paients_name"
                                                                value="{{ $appointment->patients_name }}"
                                                                name="patients_name">
                                                        </div>
                                                        <div class="col">
                                                            <label for="patients_phone">Phone Number</label>
                                                            <input type="text" class="form-control"
                                                                id="patients_phone"value="{{ $appointment->phone }}"
                                                                name="phone">
                                                        </div>
                                                    </div>
                                                    <div class="row ">
                                                        <div class="col-md-6 form-group mt-3">
                                                            <label for="date">Date</label>
                                                            <input type="date" name="date"
                                                                class="form-control datepicker" id="date"
                                                                value="{{ $appointment->appointment_date }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Update</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>

        </section>
    </div>

    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Launch demo modal
    </button>




</x-backend.layouts.master>
