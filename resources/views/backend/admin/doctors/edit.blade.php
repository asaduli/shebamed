<x-backend.layouts.master>
  <a href="{{route('doctors.index')}}" class="btn btn-primary btn-bg mb-3">Doctor List</a>
@php 
$gender;
if($doctor->profile->gender==2){
    $gender='Female';
}
else{
    $gender='Male';
}
@endphp

<form  action="{{route('doctor.update',['id'=>$doctor->id])}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('patch')
       
        <div class="row" style="margin-top:10px; margin-bottom: 10px;">
            <div class="col">
                <label>First Name: *</label>
                <input type="text" value="{{$doctor->first_name}}"name="fname" class="form-control" placeholder="First name" aria-label="First name">
            </div>
            <div class="col">
                <label> Last Name: *</label>
                <input type="text" name="lname" value="{{$doctor->last_name}}" class="form-control" placeholder="Last name" aria-label="Last name">
            </div>
        </div>
      
        <div class="row" style="margin-bottom: 10px;">
            <div class="col">
                <label for="email">Email: *</label>
                <input name="email" type="email" class="form-control" aria-label="email" value="{{$doctor->email}}">
            </div>
            <div class="col">
                <label for="phone">Phone:</label>
                <input name="phone" type="number" class="form-control" placeholder="(+880)" aria-label="phone" value="{{$doctor->profile->phone}}">
            </div>
        </div>

     

        <div class="row" style="margin-bottom:10px">

            <div class="col">
                <div class="mb-3">
                    <label for="nid" class="form-label">NID</label>
                    <input name="nid" type="number" class="form-control" id="nid" value="{{$doctor->profile->nid}}">
                </div>

            </div>
           

        </div>

        <div class="row" style="margin-bottom: 10px;">
            <div class="col">
                <label for="degree">Degree:*</label>
                <input name="degree" type="text" class="form-control" placeholder="Enter Degree" aria-label="degree" value="{{$doctor->profile->degree}}">
            </div>
           

        </div>



        <div class="row" style="margin-bottom: 10px;">

            <div class="mb-3">
                <label for="exampleFormControlTextarea1" class="form-label">Address</label>
                <textarea  name="address" class="form-control" id="exampleFormControlTextarea1" rows="3" >{{$doctor->profile->address}}</textarea>
            </div>

        </div>
        <div class="mb-3">
            <label for="exampleFormControlTextarea1" class="form-label">Bio</label>
            <textarea name="bio" class="form-control" id="exampleFormControlTextarea1" rows="3">{{$doctor->profile->bio}}</textarea>
        </div>

        
        <div class="row justify-content-center">
            <div class="col-auto">
                <button type="submit" class="btn btn-primary" style="width: 200px; font-weight: bold; font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif; margin-bottom: 50px; margin-top: 20px;">update</button>

            </div>
            <br>

        </div>




    </form>
      

</x-backend.layouts.master>